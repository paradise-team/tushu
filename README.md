# 图书管理系统

## 环境

node.js    jdk 8.0  vue.js 2.0

## 技术选用

前端使用Vue+Elementui建立用户视图，使用Springboot+mybatispuls+mysql 提供接口。

前端通过使用axios调用接口获取数据，从而渲染页面。

## 概述

1.前端可以将vue打包部署到nginx服务器 ，复制dist下面的文件放到nginx下面的html下面

2.vue打包 `npm run build` nginx启动 `start nginx`  nginx关闭 `nginx -s stop`

3.在后端项目可以在target下面找到library的jar包，直接`java  -jar libraryxx.jar`启动后台。

4.在后端项目中可以找到1.sql数据库直接注入。

5.访问 http://localhost:80/#/stuLog进入主页

(前端可以使用vue自带的server启动`npm run dev` 访问:http://localhost:8080/#/stuLog)

本项目用到的接口文档地址 ：  https://easydoc.xyz/s/22380838/147sqf8t/fNZcH7xx

前端项目地址为：  https://gitee.com/wodedanjia/library_management_project/tree/master



