package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Student;
import org.springframework.stereotype.Repository;

@Repository
public interface Studentmapper extends BaseMapper<Student> {
}
