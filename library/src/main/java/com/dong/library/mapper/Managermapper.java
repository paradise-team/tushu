package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Manager;
import org.springframework.stereotype.Repository;

@Repository
public interface Managermapper extends BaseMapper<Manager> {
}
