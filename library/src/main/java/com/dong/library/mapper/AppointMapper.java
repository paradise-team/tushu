package com.dong.library.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dong.library.entity.Appoint;
import org.springframework.stereotype.Repository;

@Repository
public interface AppointMapper extends BaseMapper<Appoint> {
}
