package com.dong.library.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
public class Brrow {
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @TableId(type= IdType.AUTO)
    private Integer id;

    private String stuId;
    private String bookId;
    private String brrowDate;
    private String returnDate;
    private String bookName;

//  借阅，还书，续借审核   0 默认  1 提交申请   2 同意申请
    private Integer brrCheck;
    private Integer retCheck;
    private Integer addCheck;
}
